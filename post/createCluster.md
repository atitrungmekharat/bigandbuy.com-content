+++
author = "Atit"
title = "How to Create Kubernetes Cluster in DigitalOcean"
date = "2020-12-15"
description = "How to Create Kubernetes Cluster in DigitalOcean"
tags = [
    "kubernetes",
    "digitalocean"
]
+++

In the **Create** menu, click **Clusters** to go to the **Create a Cluster** page. On this page, you'll choose a Kubernetes version, datacenter region, and cluster capacity for your cluster and then create it.
